#!/bin/bash

script_name_full="$(basename ${0})"
script_name_base="${script_name_full%%.*}"
script_file_full="$(realpath -s ${0})"
script_file_base="${script_file_full%%.*}"
config_file_full="${script_file_base}.conf"

log_file_enabled=0

#################################### Start of functions ############################################

function log_prefix() {
  echo "$(date +%F" "%T) $(hostname)"
}

function log_info() {
  local prefix="$(log_prefix)"
  echo "${prefix} INFO  ${1}"
  [ "${log_file_enabled}" -eq 1 ] && echo "${prefix} INFO  ${1}" >> "${log_file}"
}

function log_error() {
  local prefix="$(log_prefix)"
  echo "${prefix} ERROR ${1}"
  [ "${log_file_enabled}" -eq 1 ] && echo "${prefix} ERROR ${1}" >> "${log_file}" 
}

## Change the maintenance mode of NextCloud using occ
function nc_maintenance_mode() {
  log_info "Setting Nextcloud maintenance mode ${1}"
  sudo -u "${nc_web_user}" php "${nc_web_root}/occ" maintenance:mode --"${1}" >> "${log_file}" 2>&1
  if [ "$?" -ne 0 ]; then
    log_info "Cannot set Nextcloud maintenance mode to ${1}."
    return 1
  else
    return 0
  fi
}

function cleanup() {
  # remove SQL dump file and directory
  rm -rf "${db_dump_dir}" >> "${log_file}" 2>&1
  nc_maintenance_mode off
}

function check_config() {
  # Check if the log file can be created
  if [ -z "${log_file+x}" ] || [ ! -w "${log_file}" ]; then
    log_error "ERROR: Log file '${log_file}' cannot be opened for writing."
    return 1
  fi
  # Check if the web root folder exists
  if [ -z "${nc_web_root+x}" ] || [ ! -d "${nc_web_root}" ]; then
    log_error "ERROR: Nextcloud web root folder '${nc_web_root}' does not exist."
    return 1
  fi
  # Check if the nextcloud data folder exists
  if [ -z "${nc_data_dir+x}" ] ||  [ ! -d "${nc_data_dir}" ]; then
    log_error "Nextcloud data folder '${nc_data_dir}' does not exist. "
    return 1
  fi
  # Check if the web server user exist
  local user_exists=$(id -u "${nc_web_user}" > /dev/null 2>&1; echo $?)
  if [ -z "${nc_web_user+x}" ] ||  [ "${user_exists}" -ne 0 ]; then
    log_error "Web server user '${nc_web_user}' does not exist."
    return 1
  fi
  # Check if the borg repository folder exists
  if [ -z "${borg_repo+x}" ] || [ ! -d "${borg_repo}" ]; then
    log_error "Borg respository folder '${borg_repo}' does not exist."
    return 1
  fi
}

function load_config() {
  log_info "Configuration file: '${config_file_full}'"
  if [ ! -f "${config_file_full}" ]; then
    log_error "Configuration file '${config_file_full}' does not exist"
    return 1
  fi
  source "${config_file_full}"
}

##################################### End of functions #############################################

log_info "Nextcloud backup script"

load_config
if [ "$?" -ne 0 ]; then
  exit 1
fi

check_config
if [ "$?" -ne 0 ]; then
  exit 1
fi

log_info "Log file: '${log_file}'"
log_file_enabled=1

# Setting this, so you won't be asked for your repository passphrase:
#export BORG_PASSPHRASE='XYZl0ngandsecurepa_55_phrasea&&123'
# or this to ask an external program to supply the passphrase:
#export BORG_PASSCOMMAND='pass show backup'

## If not running as root, display error on console and exit
if [ $(id -u) -ne 0 ]; then
  echo "This script MUST be run as ROOT. Exiting ..."
  exit 2
fi

log_info "---------------- Starting Nextcloud backup ----------------"

nc_maintenance_mode on
if [ "$?" -ne 0 ]; then
  cleanup
  exit 2
fi

db_dump_dir=$(mktemp -d)
if [ "$?" -ne 0 ]; then
  log_error "Cannot create temporary directory for MySQL dump."
  cleanup
  exit 2
fi

borg_paths+=("${db_dump_dir}")

db_dump_file_full="${db_dump_dir}/$(hostname)-nextcloud-db.sql-$(date +"%Y-%m-%d-%H-%M-%S")"

log_info "Dumping MySQL database to '${db_dump_file_full}'"

export MYSQL_PWD="${nc_db_passwd}" # This is to avoid the warning message due to CLI password

mysqldump \
  --single-transaction \
  -h "${nc_db_host}" \
  -u "${nc_db_user}" \
  "${nc_db_name}" > "${db_dump_file_full}"

if [ "$?" -ne 0 ]; then
  log_error "Cannot dump MySQL database."
  cleanup
  exit 2
fi

log_info "MySQL dump size: $(stat --printf='%s' "${db_dump_file_full}" | numfmt --to=iec)"
log_info "Starting borg create..."

borg create \
  --verbose \
  --filter AME \
  --stats \
  --show-rc \
  --compression lz4 \
  "${borg_repo}"::'{hostname}-{now:%Y-%m-%d-%H%M%S}' \
  "${borg_paths[@]/#/}" \
  --exclude-caches \
  "${borg_exclude_patterns[@]/#/-e }" \
  >> "${log_file}" 2>&1

backup_exit="$?"

if [ "${backup_exit}" -ne 0 ]; then
  log_error "Cannot create borg backup."
  cleanup
  exit 2
fi

log_info "borg create completed."
log_info "Starting borg prune..."

borg prune \
  --verbose \
  --list \
  --prefix '{hostname}-' \
  --show-rc \
  "${borg_prune_keep_args[@]/#/}" \
  "${borg_repo}" \
  >> "${log_file}" 2>&1

prune_exit="$?"

if [ "${prune_exit}" -ne 0 ]; then
  log_error "Cannot prune borg backup."
  cleanup
  exit 2
fi

log_info "borg prune completed."
cleanup
log_info "--------------- Nextcloud backup complete ---------------"
